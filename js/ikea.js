var cosas = ["Reloj de arena decorativo	TILLSYN,descripcion del objeto,https://www.ikea.com/PIAimages/0460829_PE607058_S5.JPG,10,5",
"LATTJO  Peluche,descripcion del objeto,https://www.ikea.com/PIAimages/0710666_PE727700_S5.JPG,12,6",
"DRAGON  Cubertería 24 piezas,descripcion del objeto,https://www.ikea.com/PIAimages/0131001_PE285442_S5.JPG,22,2"];
var jsonArticulos = []; //Almacenará, como array de objetos, todos los artículos que se reciban.
for(i=0; i < cosas.length; i++){
    // Transformo el contenido de cosas a un json añadiendole un id a cada artículo.
    var elemento = cosas[i].split(",");
    var nuevoElemento = {"idProducto":i, "nombre":elemento[0], "descripcion":elemento[1], "imagen": elemento[2], "precio": elemento[3], "stock": elemento[4]};
    jsonArticulos.push(nuevoElemento);
}

var ArticuloCesta = function (idProducto, nombre, precio, stock, unidadesCompradas, unidadesCompradasTotal, posJsonArticulos, posJsonCesta){
    /* 
    *   Clase: Se encarga de la gestión de los artículos incluidos en la cesta.
    *
    *   Recibe:  
    *       idProducto: (integer) es el identificador del producto para poder localizarlo.
    *       nombre: (string) el nombre del artículo.
    *       precio: (float) el precio del artículo.
    *       stock: (integer) las unidades que quedan del artículo.
    *       unidadesCompradas: (integer) las unidades que se acaban de comprar.
    *       unidadesCompradasTotal: (integer) cuantas unidades se han comprado en total de esté artículo.
    *       posJsonArticulos: (integer || null) la posición del artículo en el jsonArticulos.
    *       posJsonCesta: (integer || null) la posición del artículo en el arrayCesta.
    * 
    *   Métodos:
    *       (público) crearArticuloCesta
    *       (público) actualizarArticuloCesta
    */
   
    this.idProducto = idProducto; 
    this.nombre = nombre;
    this.precio = precio;
    this.stock = stock;
    this.unidadesCompradas = unidadesCompradas;
    this.unidadesCompradasTotal = unidadesCompradasTotal;
    this.posJsonArticulos = posJsonArticulos;
    this.posJsonCesta = posJsonCesta;

    this.crearArticuloCesta = function (){
        /*
        *   Método(público)
        *
        *   Realiza:
        *       Añade un nuevo elemento debidamente formado tienda.cesta.jsonCesta.
        *       Llama al Método pintarCesta de la clase Cesta Ikea para que se actualize el DOM.
        */
        this.stock = this.stock - this.unidadesCompradas;
        var nuevoElemento = {"idProducto":this.idProducto, "nombre":this.nombre, "precio": this.precio, "stock": this.stock, "unidadesCompradasTotal": this.unidadesCompradasTotal};
        tienda.cesta.jsonCesta.push(nuevoElemento);
        tienda.cesta.pintarCesta();
    }

    this.actualizarArticuloCesta = function (){
        /*
        *   Método(privado)
        *
        *   Realiza:
        *       Busca en tienda.cesta.jsonCesta el artículo y le modifica los datos.
        *       Llama al Método pintarCesta de la clase Cesta Ikea para que se actualize el DOM.
        */
        this.stock = this.stock - this.unidadesCompradas;
        this.unidadesCompradasTotal = parseInt(tienda.cesta.jsonCesta[posJsonCesta].unidadesCompradasTotal) + parseInt(this.unidadesCompradas);
        tienda.cesta.jsonCesta[posJsonCesta].stock == this.stock;
        tienda.cesta.jsonCesta[posJsonCesta].unidadesCompradasTotal = this.unidadesCompradasTotal; 
        tienda.cesta.pintarCesta();
    }
}

var CestaIkea = function (){
    /* 
    *   Clase: Se encarga de la gestión de la cesta y de renderizarla en el DOM.
    * 
    *   Métodos:
    *       (público) anadirCesta
    *       (público) pintarCesta
    */

    this.jsonCesta = [];
    

    this.anadirCesta = function (idProducto, nombre, precio, stock, unidadesCompradas, unidadesCompradasTotal, posJsonArticulos, posJsonCesta){
        /*
        *   Método(público)
        *
        *   Recibe:  
        *       idProducto: (integer) es el identificador del producto para poder localizarlo.
        *       nombre: (string) el nombre del artículo.
        *       precio: (float) el precio del artículo.
        *       stock: (integer) las unidades que quedan del artículo.
        *       unidadesCompradas: (integer) las unidades que se acaban de comprar.
        *       unidadesCompradasTotal: (integer) cuantas unidades se han comprado en total de esté artículo.
        *       posJsonArticulos: (integer || null) la posición del artículo en el jsonArticulos.
        *       posJsonCesta: (integer || null) la posición del artículo en el arrayCesta.
        *
        *   Realiza:
        *       Crea un objeto de la clase ArticuloCesta.
        *       Comprueba si el artículo ya esta en la cesta y en función del resultado llama al método que corresponda de 
        *       la calse ArticuloCesta.
        */

       var nuevoArticuloCesta = new ArticuloCesta(idProducto, nombre, precio, stock, unidadesCompradas, unidadesCompradasTotal, posJsonArticulos, posJsonCesta);
        if(posJsonCesta==null){            
            nuevoArticuloCesta.crearArticuloCesta();
        }else{
            nuevoArticuloCesta.actualizarArticuloCesta();
        }
    }

    this.pintarCesta = function (){
        /*
        *   Método(público)
        *
        *   Realiza:
        *       Mira si hay que mostrar o no la cesta en función del contenido del this.jsonCesta.
        *       Vacia el contenido de la cesta y lo vuelve a pintar con los últimos datos.
        */
        var donde = document.querySelector('#cesta');
        if(this.jsonCesta.length > 0 && donde.classList.contains("invisible")){donde.classList.remove("invisible");}
        if(this.jsonCesta.length == 0 && !donde.classList.contains("invisible")){donde.classList.add("invisible");}
        donde.innerHTML="";
        for(i=0; i < this.jsonCesta.length; i++){
            //recorremos this.jsonCesta para añador artículo por artículo al DOM de la cesta.
            var articuloCesta = document.querySelector('template#itemsCesta').content.cloneNode(true);
            articuloCesta.querySelector("div").setAttribute('id', 'elementoCesta_'+this.jsonCesta[i].idProducto);
            var spans = articuloCesta.querySelectorAll("span");
            var precioTotal = parseFloat(this.jsonCesta[i].precio) * parseFloat(this.jsonCesta[i].unidadesCompradasTotal);
            spans[0].textContent = this.jsonCesta[i].nombre;
            spans[1].textContent = this.jsonCesta[i].precio + " €";
            spans[2].textContent = this.jsonCesta[i].unidadesCompradasTotal;
            spans[3].textContent = "= " + precioTotal + " €";
            donde.appendChild(articuloCesta);
        }
    }
}

var ProductoIkea = function (idProducto, nombre, descripcion, imagen, precio, stock){
    /* 
    *   Clase: Se encarga de la gestión de los artículos que se reciben para renderizarlos en el DOM y actualizarlos.
    *
    *   Recibe:  
    *       idProducto: (integer) es el identificador del producto para poder localizarlo.
    *       nombre: (string) el nombre del artículo.
    *       descripcion: (string) la descripcion del artículo. 
    *       imagen: (string) la url donde se puede conseguir el recurso.
    *       precio: (float) el precio del artículo.
    *       stock: (integer) las unidades que quedan del artículo.
    * 
    *   Métodos:
    *       (público) pintarArticulo
    *       (público) actualizarArticulo
    *       (privado) __clickComprar
    */

    this.idProducto = idProducto; 
    this.nombre = nombre;
    this.descripcion = descripcion;
    this.imagen = imagen;
    this.precio = precio;
    this.stock = stock;

    this.pintarArticulo = function (){
        /*
        *   Método(público)
        *
        *   Realiza:
        *       Pinta en el DOM los productos que se le pasan a la clase. 
        */    

        var donde = document.querySelector('#colItems');
        var articulo = document.querySelector('template#item').content.cloneNode(true);
        articulo.querySelector("img").src=this.imagen;
        articulo.querySelector("h4").textContent = this.nombre;
        articulo.querySelector("h5").textContent = this.precio+" €";
        articulo.querySelector("p").textContent = "Quedan " + this.stock + " unidades.";
        articulo.querySelector("button").setAttribute('id', 'compra_'+this.idProducto);
        if(this.stock==0){
            articulo.querySelector("img").parentNode.style.filter = "grayscale(100%) opacity(50%)";
            articulo.querySelector("button").disabled = true;
        }
        //articulo.querySelector("button").onclick = tienda.clickComprar;
        articulo.querySelector("button").onclick = this.__clickComprar;
        donde.appendChild(articulo);
    }

    this.__clickComprar = function(){
        /* 
        *   Método(privado)
        *
        *   Realiza
        *       Recorre el jsonArticulos y coge los datos del que haya sido comprado.
        *       Crea un objeto de la clase ProductoIkea con esos datos y ese objeto llama a su Método actualizarArticulo para que quede actualizado.
        * 
        *       Mira si la cesta de la compra tiene algun producto.
        *       En caso de que no esté coge los datos necesarios del jsonArticulos
        *       Si los tiene los coge del arrayCestaCompra
        *       Crea un objeto de la clase CestaIkea con esos datos y llama a su Método anadirCesta para añadir o modificar el artículo en la cesta. 
        *       
        */
       var id = this.id;
       var idProducto =  id.substring(7);;
       var unidadesCompradas = 1;
       var posJsonArticulos = null;
       var posJsonCesta = null;
       
       for(i=0; i < tienda.jsonArticulos.length; i++){
            if(tienda.jsonArticulos[i].idProducto == idProducto){
                // Recorre tienda.jsonArticulos y crea un objeto de cada uno de ellos.
                posJsonArticulos = i;
                var producto = new ProductoIkea(tienda.jsonArticulos[i].idProducto, tienda.jsonArticulos[i].nombre, tienda.jsonArticulos[i].descripcion, tienda.jsonArticulos[i].imagen, tienda.jsonArticulos[i].precio, tienda.jsonArticulos[i].stock);
                producto.actualizarArticulo(unidadesCompradas, posJsonArticulos);
            }
        }
        
        for(i=0; i < tienda.cesta.jsonCesta.length; i++){
            if(tienda.cesta.jsonCesta[i].idProducto == idProducto){
                // Si el artículo está en la cesta cogemos los datos de ahí.
                posJsonCesta = i;
                var nombre = tienda.cesta.jsonCesta[i].nombre;
                var precio = tienda.cesta.jsonCesta[i].precio;
                var stock = tienda.cesta.jsonCesta[i].stock;
                var unidadesCompradasTotal = tienda.cesta.jsonCesta[i].unidadesCompradasTotal + unidadesCompradas;
            }
        }
    
        if(posJsonCesta==null){
            // Si el artículo no esta en la cesta cogemos los datos de tienda.jsonArticulos
            var nombre = tienda.jsonArticulos[idProducto].nombre;
            var precio = tienda.jsonArticulos[idProducto].precio;
            var stock = tienda.jsonArticulos[idProducto].stock;
            var unidadesCompradasTotal = unidadesCompradas;
        }
                
        tienda.cesta.anadirCesta(idProducto, nombre, precio, stock, unidadesCompradas, unidadesCompradasTotal, posJsonArticulos, posJsonCesta);
        
    }
   
    this.actualizarArticulo = function(unidadesCompradas, posJsonArticulos){
        /*
        *   Método(público)
        *
        *   Recibe:
        *       unidadesCompradas: (integer) es la cantida de artículos que se han comprado.
        *       posJsonArticulos: (integer) es la posición del artículo en su json.
        * 
        *   Realiza:
        *       Actualiza el stock del artículo en el jsonArticulos.
        *       Elimina los productos pintados en el DOM y llama a la funcion pintarProductos para que vuelva a pintarlos.
        */
        this.stock = parseInt(this.stock) - parseInt(unidadesCompradas);
        tienda.jsonArticulos[posJsonArticulos].stock = this.stock;
        var donde = document.querySelector("#colItems");
        var divs = document.querySelectorAll("#colItems > div");
        for(i=0; i < divs.length; i++){
            donde.removeChild(divs[i]);
        }

        tienda.__crearArticulo();
    }
}

var TiendaIkea = function(articulos){
    /* 
    *   Clase: Se encarga de instanciar la clase ProductosIkea, crear los objetos artículo necesarios y de instanciar la clase CestaIkea 
    *
    *   Recibe:  
    *       articulos: (array) Array de objetos con los artículos.
    * 
    *   Métodos:
    *       (privado) __crearArticulo.
    *       (privado) __instanciarCesta.
    */
    this.jsonArticulos = articulos;
    this.cesta;

    this.__crearArticulo = function(){
        /* 
        *   Método(privado)
        *
        *   Realiza
        *       Recorre this.jsonArticulos para crear un objeto de la clase ProductoIkea de cada uno de los elementos contenidos.
        *       Utiliza el Método pintarArticulo para pintar cada objeto creado en el DOM.
        */
        for(i=0; i < this.jsonArticulos.length; i++){
            var articulo = new ProductoIkea(this.jsonArticulos[i].idProducto, this.jsonArticulos[i].nombre, this.jsonArticulos[i].descripcion, this.jsonArticulos[i].imagen, this.jsonArticulos[i].precio, this.jsonArticulos[i].stock);
            articulo.pintarArticulo();
        }
    };
    
    this.__instanciarCesta = function(){
        /* 
        *   Método(privado)
        *
        *   Realiza
        *       Crea un objeto cesta instanciando la clase CestaIkea.
        */

        this.cesta = new CestaIkea();
    };

    this.__crearArticulo();
    this.__instanciarCesta();
}

var tienda = new TiendaIkea(jsonArticulos); //Creo un objeto tienda instanciando la clase TiendaIkea pasandole jsonArticulos.